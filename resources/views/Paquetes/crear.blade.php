@extends('../layouts/master')

@section('contenido')
<div class="row">
    <div class="offset-md-3 col-md-6">
        <div class="card">
            <div class="card-header text-center">Añadir nuevo paquete</div>
            <div class="card-body" style="padding:30px">
                <form method="POST" action="{{ route('paquetes.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="direccion">Dirección</label>
                        <input type="text" name="direccion" id="direccion" class="form-control" value="{{old('direccion')}}" required">
                    </div><br>
                    <div class="form-group">
                        <label for="transportista_id">Transportista</label>
                        <select name="transportista_id" id="transportista_id">
                            @foreach ($transportistas as $t)
                                <option value="{{$t->id}}">{{$t->nombre}}</option>
                            @endforeach
                        </select>
                    </div><br>
                    <div class="form-group">
                        <label for="imagen">Imagen</label>
                        <input type="file" name="imagen" id="imagen" class="form-control" value="{{old('imagen')}} required">
                    </div>
                    

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Añadir paquete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection