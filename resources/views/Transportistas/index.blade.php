@extends('../layouts/master')

@section('contenido')
    <div class="row">
        @foreach($transportistas as $transportista)
            <div class="col-xs-12 col-sm-6 col-md-4">
                <a href="{{ route('transportistas.show' , $transportista) }}">
                    <img src="{{asset('assets/imagenes')}}/transportistas/{{$transportista->imagen}}" style="height:200px"/>
                    <h4 style="min-height:45px;margin:5px 0 10px 0">{{$transportista->nombre}}</h4>
                </a>
                <?php $paquetes = count($transportista->paquetes);?>
                <p>{{$paquetes}} {{$paquetes == 1 ? 'paquete pendiente' : 'paquetes pendientes'}}  de entregar</p>

            </div>
        @endforeach
    </div>
@endsection