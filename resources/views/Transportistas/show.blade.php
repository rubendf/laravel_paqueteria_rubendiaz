@extends('../layouts/master')

@section('contenido')
<div class="row">
    <div class="col-sm-3">
        <img src="{{asset('assets/imagenes')}}/transportistas/{{$transportista->imagen}}" style="height:200px"/>
    </div>
    <div class="col-sm-9">
        <h2>{{$transportista->nombre}}</h2>
        <p>Años de permiso de circulacion: {{$transportista->fecha_permiso}}</p>
        <p>Empresas:</p>
            <ul>
                @foreach ($transportista->empresas as $empresa)
                    <li>{{$empresa->nombre}}</li>
                @endforeach
            </ul>
        <br>
        @if(count($transportista->paquetes) >= 1)
            <h4>Paquetes:</h4>
            @foreach ($transportista->paquetes as $paquete)
                <li>
                    Paquete {{$paquete->id}} - {{$paquete->direccion}}: 
                    {{!$paquete->entregado ? 'pendiente de entrega' : 'entregado'}}
                </li>
            @endforeach
        @endif
        <br><br>

        <a href="{{ route('transportistas.entregar' , $transportista) }}">Entregar todo</a>
        <a href="{{ route('transportistas.noentregado' , $transportista) }}">Marcar todo como no entregado</a>
        
       
    </div>
</div>
@endsection