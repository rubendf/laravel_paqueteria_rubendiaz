<?php

namespace App\Http\Controllers;

use App\Models\Transportista;
use Illuminate\Http\Request;

class TransportistaController extends Controller
{
    public function index(){
        $transportistas = Transportista::all();
        
        return view('Transportistas.index', compact('transportistas'));
    }

    public function show(Transportista $transportista){
        
        return view('Transportistas.show', compact('transportista'));
    }

    public function entregar(Transportista $transportista){

        foreach($transportista->paquetes as $paquete){
            $paquete->entregado = 1;
            $paquete->save();
        }

        return view('Transportistas.show', compact('transportista'));

    }

    public function noentregado(Transportista $transportista){
        foreach($transportista->paquetes as $paquete){
            $paquete->entregado = 0;
            $paquete->save();
        }

        return view('Transportistas.show', compact('transportista'));
        
    }
}
