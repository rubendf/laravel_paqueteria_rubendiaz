<?php

namespace App\Http\Controllers;

use App\Models\Paquete;
use App\Models\Transportista;
use Illuminate\Http\Request;

class PaquetesController extends Controller
{
    public function crear(){
        $transportistas = Transportista::all();
        return view('Paquetes.crear', compact('transportistas'));
    }

    public function store(Request $request){
        if($request->hasFile('imagen')){
            /*$request->imagen->store(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix());*/
            //$request->imagen = $request->imagen->store('', 'paquetes');
            //$request->offsetUnset('imagen');
        }

        $paquete = Paquete::create($request->all());

        if(!$paquete->save()){
            redirect()->back()->with('alert-danger', 'Ha ocurrido un error');
        }
        return redirect()->route('transportistas.index');
    }
}
