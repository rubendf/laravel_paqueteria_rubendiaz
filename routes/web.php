<?php

use App\Http\Controllers\PaquetesController;
use App\Http\Controllers\TransportistaController;
use App\Models\Transportista;
use Illuminate\Support\Facades\Route;
use Mockery\Generator\StringManipulation\Pass\Pass;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', [TransportistaController::class, 'index'])->name('transportistas.index');

Route::get('/transportistas/{transportista}', [TransportistaController::class, 'show'])->name('transportistas.show');

Route::get('/transportistas/{transportista}/entregar', [TransportistaController::class, 'entregar'])->name('transportistas.entregar');

Route::get('/transportistas/{transportista}/noentregado', [TransportistaController::class, 'noentregado'])->name('transportistas.noentregado');


Route::get('/paquetes/crear', [PaquetesController::class, 'crear'])->name('paquetes.crear');

Route::post('/paquetes/store', [PaquetesController::class, 'store'])->name('paquetes.store');
